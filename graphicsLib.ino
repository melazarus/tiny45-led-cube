
/*
  Set all bits to 0 to turn off all LED's
 */
void clearCube()
{
  for(char i=0; i<BUFFERSIZE; i++) buffer[i] = 0;
}

/*
  Invert all bits in the buffer array.
 */
void invertCube()
{
  for(byte i = 0; i<BUFFERSIZE; i++) buffer[i] = ~buffer[i]; 
}

/*
  (Re)Set the pixel at cordinate x,y,z.
 */
void setPixel(byte x, byte y, byte z, boolean value)
{
  byte pos = x+(y*CUBESIZE);
  buffer[pos] |= (1 << z);
  if(value) return;
  buffer[pos] &= ~(1 << z);
}

/*
  set or clear an edge on a given axis.
 offset1 and offset2 are the offsets in the other 2 axes
 x => y,z;
 y => x,z;
 z => x,y;
 */
void setEdge(byte axis, byte offset1, byte offset2, boolean value)
{
  byte x = offset1;
  byte y = offset1;
  byte z = offset2;

  if(axis == zAxis) y=offset2;

  for(byte i=0; i<CUBESIZE; i++)
  {
    if (axis == xAxis) x = i;
    else if (axis == yAxis) y = i;
    else z = i;
    setPixel(x,y,z,value);
  }
}

/*
  set or clear a Face on a given axis.
 offset is the offset on the same axis.
 x: draw vertical (Left to right) face
 y: draw vertical (Front to back) face
 z: draw horizontal face
 */
void setFace(byte axis, byte offset, boolean value)
{  
  if(axis==zAxis)
    connectEdge(((axis+1)%3),0, offset, CUBESIZE-1, offset, value);
  else
    connectEdge(((axis+2)%3), offset,0, offset, CUBESIZE-1, value);
}

/*
  connect 2 edges to form a face (also usable for diagonal faces)
 offset 1&2 are used to indicate the first edge
 offset 3&4 are used to locate the second edge
 */
void connectEdge(byte axis, byte offset1, byte offset2, byte offset3, byte offset4, boolean value)
{  
  char adder1 = calculateDirection(offset1,offset3);
  char adder2 = calculateDirection(offset2,offset4);
  ;

  for(byte i=0; i<CUBESIZE; i++)
  {
    setEdge(axis, offset1, offset2, value);
    offset1+=adder1;
    offset2+=adder2;
  }
}

char calculateDirection(byte a, byte b)
{
  if(a==b) return 0;
  if(a<b) return 1;
  return -1;
}



