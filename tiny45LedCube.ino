#define DATA 2
#define CLK 0
#define LATCH 1

#define CUBESIZE 3
#define BUFFERSIZE CUBESIZE*CUBESIZE

#define xAxis 0
#define yAxis 1
#define zAxis 2

volatile byte layer = 0;
byte buffer[BUFFERSIZE];

void setup() {  
  DDRB |= (1 << DATA) | (1 << CLK) | (1 << LATCH);
  setupTimer();
}

void setupTimer(){
  //don't use timer0, it is used by delay, millis, micros etc..
  cli();
  TCCR1 = 0b00001001;  
  GTCCR = 0b00000000;
  TIMSK |= _BV(OCIE1A); //Don't be stupid and leave the other bits alone
  PLLCSR =0b00000000;
  OCR1A = 255; //This value seems to be ignored
  sei();
}

void loop() {  
  //TODO: rain drops
  //TODO: fall down/fall up
  //todo: star field
  //todo: random on/random off
  //todo: fire works
  //todo: dice

for(byte i=0; i< 3; i++){
  setFace(xAxis,i,1);
  setFace(yAxis,i,1);
  setFace(zAxis,i,1);
  delay(500);
  clearCube();
}
}

















