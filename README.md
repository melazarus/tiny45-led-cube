# README #

This is the source code to drive LED cubes.

The goal is to provide a fast, light framework to drive LED cubes of various sizes.

Current version is tested/run on a attiny45 at 16Mhz with 27 LED's (3x3x3)

### Features ###
This is a list of features that will be in the firware.

driver, basic and animations are (will be) kept in separate files.

* Basic
** clearCube
** invertCube
** setPixel
** drawLine
** drawRectangle
* animations
** Moving triangles
** Random pixel fill
** snake
** pattern strobe

### Contribution guidelines ###

If you have ideas on how to make the code Smaller, Faster (use less CPU cycles) and cleaner please tell/do so


