#define ClockToggle PINB = 1 << CLK; PINB = 1 << CLK;
#define LatchToggle PINB = 1 << LATCH; PINB = 1 << LATCH;

/* 
 This named interrup routine attaches to the TIMER1_COMP1 vector.
 More info: Atiny45 datasheet page 48. (interrupt vectors)
 */
ISR(TIMER1_COMPA_vect)
{ 
  updateSingleLayer();
}

/*
  The update routine. Every time this executes it will update one layer on the cube.
 This function should be AS FAST as possible.
 Currently the code below takes 16 µs and runs at 250 hz. leaving 99.6% of time left for other tasks.
 */
void updateSingleLayer()
{
  // Increase the layer number we are updating. Reset to 0 after we reach the last layer.
  if(++layer>CUBESIZE-1) layer = 0;

  // First we send n bits to indicate the on/off status of each layer.
  // Only one layer should be on at a time.
  // for larger cube we may need to use a 3-to-8 decoder to ellimitate the possibility to light up multiple layers
  for(char i=CUBESIZE-1;i>=0;i--)
  {
    PORTB &= ~(1 << DATA);
    if(i==layer) PORTB |= (1 << DATA);
    ClockToggle;   
  }

  // Nexe pulse out one bit for each led in one layer.
  // Each layer represents one bit on the buffer array. The filter helps us to select the right bit.  
  byte filter = (1 << layer);
  for(byte i=0; i< BUFFERSIZE; i++)
  {
    boolean val = (buffer[BUFFERSIZE-1-i] & filter);
    PORTB &= ~(1 << DATA);
    if(val) PORTB |= (1 << DATA);
    ClockToggle;
  }

  //Send a pulse to to the shift registers to tell them to accept the current state of bits.
  LatchToggle;
}

